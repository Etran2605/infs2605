/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application.database;

import Model.Room;
import java.util.*;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author renazhou
 */
public class RoomViewQueryIT {
    
    public RoomViewQueryIT() {
    }

    /**
     * Test of getRoomsFromFile method, of class RoomViewQuery.
     */
    @Test
    public void testGetRoomsFromFile() {
        System.out.println("getRoomsFromFile");
        RoomViewQuery instance = new RoomViewQuery();
        ArrayList<Room> result = new ArrayList<Room>(instance.getRoomsFromFile());
        
        
        Room r = new Room ("26", "Double-double", "A Room with two double ( or perhaps queen) beds. May be occupied by one or more person.", 350);
        boolean found = false;
        for (Room e : result) {
            if (r.getNumber().getValue().equals(e.getNumber().getValue())) {
                found = true;
                assert(r.getName().getValue().equals(e.getName().getValue()));
                assert(r.getDescription().getValue().equals(e.getDescription().getValue()));
                assert(r.getCostPerNight().getValue().equals(e.getCostPerNight().getValue()));
            }
        }
        if (!found) fail("Didn't find room");
        
                r = new Room ("31", "King", "A room with a king sized bed. May be occupied by one or more people.", 300);
        found = false;
        for (Room e : result) {
            if (r.getNumber().getValue().equals(e.getNumber().getValue())) {
                found = true;
                assert(r.getName().getValue().equals(e.getName().getValue()));
                assert(r.getDescription().getValue().equals(e.getDescription().getValue()));
                assert(r.getCostPerNight().getValue().equals(e.getCostPerNight().getValue()));
            }
        }
        if (!found) fail("Didn't find room");
        
        r = new Room ("31", "King", "A room with a king sized bed. May be occupied by one or more people.", 300);
        found = false;
        for (Room e : result) {
            if (r.getNumber().getValue().equals(e.getNumber().getValue())) {
                found = true;
                assert(r.getName().getValue().equals(e.getName().getValue()));
                assert(r.getDescription().getValue().equals(e.getDescription().getValue()));
                assert(r.getCostPerNight().getValue().equals(e.getCostPerNight().getValue()));
            }
        }
        if (!found) fail("Didn't find room");
        

        
                r = new Room ("26", "Double-double", "A Room with two double ( or perhaps queen) beds. May be occupied by one or more person.", 350);
        found = false;
        for (Room e : result) {
            if (r.getNumber().getValue().equals(e.getNumber().getValue())) {
                found = true;
                assert(r.getName().getValue().equals(e.getName().getValue()));
                assert(r.getDescription().getValue().equals(e.getDescription().getValue()));
                assert(r.getCostPerNight().getValue().equals(e.getCostPerNight().getValue()));
            }
        }
        if (!found) fail("Didn't find room");
        
        
                r = new Room ("50", "Double-double", "A Room with two double ( or perhaps queen) beds. May be occupied by one or more person.", 350);
        found = false;
        for (Room e : result) {
            if (r.getNumber().getValue().equals(e.getNumber().getValue())) {
                found = true;
                assert(r.getName().getValue().equals(e.getName().getValue()));
                assert(r.getDescription().getValue().equals(e.getDescription().getValue()));
                assert(r.getCostPerNight().getValue().equals(e.getCostPerNight().getValue()));
            }
        }
        if (!found) fail("Didn't find room");
                
    }
    
    
    
}
