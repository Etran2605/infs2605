/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application;

import hotel.application.database.StaffQuery;
import java.net.URL;
import java.util.ResourceBundle;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author renazhou
 */
public class LoginPageControllerTest {

    public LoginPageControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testLoginTrue() {
        System.out.println("Testing with correct login details");
        String username = "rena";
        String password = "pw";
        StaffQuery sq = new StaffQuery();
        assertEquals(true, sq.login(username, password));
    }

    @Test
    public void testLoginFalse() {
        System.out.println("Testing with false login details");
        String username = "john";
        String password = "newman";
        StaffQuery sq = new StaffQuery();
        assertEquals(false, sq.login(username, password));
    }

}
