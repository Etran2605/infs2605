package hotel.application;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Model.Guest;
import Model.Reservation;
import Model.Staff;
import Model.StaffActivity;
import hotel.application.database.GuestQuery;
import hotel.application.database.StaffActivityQuery;
import hotel.application.database.StaffQuery;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Tranny
 */
public class PopUpCustomerCreateController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        staffRefCode.setText(staffQuery.getUsername());
    }    
    @FXML
    private TextField staffRefCode; 
    
    StaffQuery staffQuery = new StaffQuery();
    Staff staff;
    private ObservableList<Staff> staffData = FXCollections.observableArrayList();


    
    @FXML
    private TextField guestIDTextfield;
    
    @FXML
    private TextField fNameTextfield;
    
    @FXML
    private TextField lNameTextfield;
    
    @FXML
    private TextField emailAddTextfield;
    
    @FXML
    private TextArea postalAddTextArea;
    
    @FXML
    private TextField contactNumberTextfield;
    
    @FXML
    private Button createButton;
    
    @FXML
    private Button backButton;
    
    GuestQuery guestQuery = new GuestQuery();
    private final ObservableList<Guest> guestData = FXCollections.observableArrayList();

     StaffActivityQuery staffActivityQuery = new StaffActivityQuery();
     StaffActivity staffActivity;
     private ObservableList<StaffActivity> staffActivityData = FXCollections.observableArrayList();
     
    
    @FXML
    private void backButtonHandle(ActionEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("ReservationView.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();      
        app_stage.setScene(scene);
        app_stage.show(); 
    }
    


    @FXML
    public void onCreateButton (ActionEvent event) throws IOException {
        
           createCustomer();
           
        Parent root = FXMLLoader.load(getClass().getResource("CustomerToReservation.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);

        app_stage.show();

      
    }

        /**
     * Creates a customer record based on the entered fields in the customer create page
     *
     */
    private void createCustomer() {

        String firstName = fNameTextfield.getText();
        String lastName = lNameTextfield.getText();
        String email = emailAddTextfield.getText();     
        String address = postalAddTextArea.getText();
        String phone = contactNumberTextfield.getText();
        
        Guest entry = new Guest(firstName, lastName, email, address, phone);
        
        guestQuery.insertGuest(entry);
        guestData.add(entry);
        
        guestIDTextfield.setText(""+entry.getGuestID());
    }

}
