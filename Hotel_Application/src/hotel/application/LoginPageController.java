/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application;

import Model.Staff;
import Model.StaffActivity;
import hotel.application.database.StaffActivityQuery;
import hotel.application.database.StaffQuery;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author renazhou
 */
public class LoginPageController implements Initializable {

    @FXML
    private Label staffIdLabel;

    @FXML
    private Label passwordLabel;

    @FXML
    private TextField staffIdTextField;

    @FXML
    private PasswordField passwordPasswordField;

    @FXML
    private Button logInButton;

    StaffQuery staffQuery = new StaffQuery();
    Staff staff;
    private ObservableList<Staff> staffData = FXCollections.observableArrayList();



    @FXML
    private void handleButtonAction(ActionEvent event) {
        String staffId = staffIdTextField.getText();
        String password = passwordPasswordField.getText();

        StaffQuery sq = new StaffQuery();

        LoginPage lp1 = new LoginPage();

        if (sq.login(staffId, password) == true) {
            System.out.println("correct pw");
            
            try {

                Parent root = FXMLLoader.load(getClass().getResource("Dashboard.fxml"));
                Scene scene = new Scene(root);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.hide();
                app_stage.setScene(scene);
                app_stage.show();

            } catch (Exception e) {

            }

        } else {
            lp1.wrongPassword();

        }
    }

    @FXML
    private void createLogin() {
        String username = staffIdTextField.getText();
        Staff entry = new Staff(username);
        staffData.add(entry);
        staffQuery.insertStaff(entry);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

}
