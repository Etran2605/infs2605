/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application;

import hotel.application.database.DatabaseSetup;
import java.io.IOException;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author renazhou
 */
public class HotelApplication extends Application {

    public static void main(String[] args) {
        DatabaseSetup.setupDatabase();

        launch(args);
    }


    @Override
    public void start(Stage stage) throws IOException, Exception {
        LoginPage lp = new LoginPage();
        lp.start(stage);

    }
}
