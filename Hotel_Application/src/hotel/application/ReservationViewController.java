/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application;

import Model.Reservation;
import Model.Room;
import Model.Staff;
import Model.StaffActivity;
import hotel.application.database.DatabaseSetup;
import hotel.application.database.ReservationQuery;
import hotel.application.database.StaffActivityQuery;
import hotel.application.database.StaffQuery;
import hotel.application.database.DatabaseSetup;
import hotel.application.database.ReservationQuery;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import static java.time.temporal.TemporalQueries.localDate;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.IntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author renazhou
 */
public class ReservationViewController implements Initializable {

    Stage prevStage;

    @FXML
    TableView<Reservation> reservationTableView;

    @FXML
    TableColumn<Reservation, String> bookingIDTableColumn;

    @FXML
    TableColumn<Reservation, String> guestIDTableColumn;

    @FXML
    TableColumn<Reservation, String> roomNumberTableColumn;

    @FXML
    TableColumn<Reservation, String> numPeopleTableColumn;

    @FXML
    TableColumn<Reservation, java.util.Date> arrivalDateTableColumn;

    @FXML
    TableColumn<Reservation, java.util.Date> departureDateTableColumn;

    @FXML
    TableColumn<Reservation, String> adjustedCheckTableColumn;

    @FXML
    TableColumn<Reservation, String> freeBreakfastTableColumn;

    @FXML
    TableColumn<Reservation, Integer> depositPaidTableColumn;

    @FXML
    TableColumn<Reservation, Integer> reservationCostTableColumn;

    @FXML
    TableColumn<Reservation, Integer> amountOwingTableColumn;

    @FXML
    private Pane dashboardPaneButton;

    @FXML
    private Pane createPaneButton;

    @FXML
    private Pane updatePaneButton;

    @FXML
    private Label loggedInUser;

    private Pane deletePaneButton;

    private ReservationQuery reservationQuery = new ReservationQuery();

    private ObservableList<Reservation> resData = FXCollections.observableArrayList();

    StaffActivityQuery staffActivityQuery = new StaffActivityQuery();
    StaffActivity staffActivity;

    private ObservableList<StaffActivity> staffActivityData = FXCollections.observableArrayList();

    StaffQuery staffQuery = new StaffQuery();
    Staff staff;
    private ObservableList<Staff> staffData = FXCollections.observableArrayList();

    @FXML
    private void createHyperlinkHandle(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("NewReservationPopUp.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();

    }

    @FXML
    private void updateHyperlinkHandler(Event event) throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("UpdateReservation.fxml"));
        Parent root = (Parent) loader.load();
        UpdateReservationController controller = (UpdateReservationController) loader.getController();
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        controller.setStage(app_stage);

        UpdateReservationController controller2
                = loader.<UpdateReservationController>getController();

        controller2.initData(reservationTableView.getSelectionModel().getSelectedItem());

        Stage popup = new Stage();
        Scene scene = new Scene(root);
        popup.setScene(scene);
        popup.show();
    }

    @FXML
    private void deleteHyperlinkHandle(Event event) throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("ReservationView.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();

        Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.WINDOW_MODAL);
        Button yes = new Button("YES");
        Button no = new Button("NO");

        yes.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {

                ReservationQuery rq = new ReservationQuery();
                rq.deleteReservation(reservationTableView.getSelectionModel().getSelectedItem());

                dialogStage.close();

                app_stage.close();

                try {
                    Parent root = FXMLLoader.load(getClass().getResource("ReservationView.fxml"));
                    Scene scene = new Scene(root);
                    Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    app_stage.hide();
                    app_stage.setScene(scene);
                    app_stage.show();
                } catch (IOException ex) {
                    Logger.getLogger(ReservationViewController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

        });

        no.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                dialogStage.close();
            }

        });

        dialogStage.setScene(new Scene(VBoxBuilder.create().
                children(new Text("Are you sure you want to Delete?"), yes, no).
                alignment(Pos.CENTER).padding(new Insets(5)).build()));
        dialogStage.show();

    }

    @FXML
    private void dashboardHyperlinkHandle(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("Dashboard.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        bookingIDTableColumn.setCellValueFactory(new PropertyValueFactory<>("ID"));
        guestIDTableColumn.setCellValueFactory(new PropertyValueFactory<Reservation, String>("guestID"));
        numPeopleTableColumn.setCellValueFactory(new PropertyValueFactory<Reservation, String>("numPeople"));
        roomNumberTableColumn.setCellValueFactory(new PropertyValueFactory<Reservation, String>("roomNumber"));
        arrivalDateTableColumn.setCellValueFactory(new PropertyValueFactory<Reservation, java.util.Date>("checkIn"));
        departureDateTableColumn.setCellValueFactory(new PropertyValueFactory<Reservation, java.util.Date>("checkOut"));
        adjustedCheckTableColumn.setCellValueFactory(new PropertyValueFactory<Reservation, String>("adjustedCheck"));
        freeBreakfastTableColumn.setCellValueFactory(new PropertyValueFactory<Reservation, String>("freeBreakfast"));
        depositPaidTableColumn.setCellValueFactory(new PropertyValueFactory<Reservation, Integer>("depositPaid"));
        reservationCostTableColumn.setCellValueFactory(new PropertyValueFactory<Reservation, Integer>("resCost"));
        amountOwingTableColumn.setCellValueFactory(new PropertyValueFactory<Reservation, Integer>("amountOwing"));

        resData.addAll(reservationQuery.getReservation());
        reservationTableView.setItems(resData);

        loggedInUser.setText(staffQuery.getUsername());
    }

    void setStage(Stage oldStage) {
        Stage prevStage = oldStage;
    }
}
