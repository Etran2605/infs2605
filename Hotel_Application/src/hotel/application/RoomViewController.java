package hotel.application;

import Model.Room;
import Model.Staff;
import hotel.application.database.ReservationQuery;
import hotel.application.database.RoomViewQuery;
import hotel.application.database.StaffQuery;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author renazhou
 */
public class RoomViewController implements Initializable {

    @FXML
    private TableView<Room> roomTableView;

    @FXML
    private TableColumn<Room, String> numberColumn;

    @FXML
    private TableColumn<Room, String> nameColumn;

    @FXML
    private TableColumn<Room, String> descColumn;

    @FXML
    private TableColumn<Room, Integer> costColumn;

    @FXML
    private Label filterByLabel;

    @FXML
    private DatePicker checkInDatePicker;

    @FXML
    private DatePicker checkOutDatePicker;

    @FXML
    private ComboBox filterComboBox;

    @FXML
    private Pane dashboardButton;

    @FXML
    private Label loggedInUser;

    StaffQuery staffQuery = new StaffQuery();
    Staff staff;
    private ObservableList<Staff> staffData = FXCollections.observableArrayList();

     /**
     * Filters the rooms by the selected room type from the combobox
     */
    @FXML
    private void filterBySelected(ActionEvent event) {
        FilteredList<Room> filteredData = new FilteredList<>(roomData, p -> true);
        filteredData.setPredicate(room -> {
            return room.getName().getValue().equals(filterComboBox.getValue());

        }
        );

        roomTableView.setItems(filteredData);
    }

    private ObservableList<Room> roomData = FXCollections.observableArrayList();

     /**
     * Filters the rooms by availability based on a given check in and check out date
     */
    @FXML
    private void filterByDate(ActionEvent event) {
        ReservationQuery resQuery = new ReservationQuery();
        LocalDate checkIn = checkInDatePicker.getValue();
        java.util.Date checkInDate = java.sql.Date.from(checkIn.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

        LocalDate checkOut = checkOutDatePicker.getValue();
        java.util.Date checkOutDate = java.sql.Date.from(checkOut.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

        List<String> bookedRoomsList = resQuery.getBookedRooms(new java.sql.Date(checkInDate.getTime()), new java.sql.Date(checkOutDate.getTime()));

        FilteredList<Room> filteredData = new FilteredList<>(roomData, p -> true);
        filteredData.setPredicate(room -> {
            return !bookedRoomsList.contains(room.getNumber().getValue());

        }
        );

        roomTableView.setItems(filteredData);

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        numberColumn.setCellValueFactory(cellData -> cellData.getValue().getNumber());
        nameColumn.setCellValueFactory(cellData -> cellData.getValue().getName());
        descColumn.setCellValueFactory(cellData -> cellData.getValue().getDescription());
        costColumn.setCellValueFactory(cellData -> cellData.getValue().getCostPerNight().asObject());

        RoomViewQuery roomViewQuery = new RoomViewQuery();
        roomData.addAll(roomViewQuery.getRoomsFromFile());
        roomTableView.setItems(roomData);

        //comboBox values
        ObservableList<String> options
                = FXCollections.observableArrayList(
                        "Single",
                        "Twin",
                        "Double",
                        "Suite",
                        "Queen ",
                        "King",
                        "Double-double"
                );
        filterComboBox.setItems(options);

        loggedInUser.setText(staffQuery.getUsername());
    }

    @FXML
    private void dashboardHandle(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("Dashboard.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();
    }
}
