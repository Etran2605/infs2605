/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Tranny
 */
public class NewReservationPopUpController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
    }    
    Stage prevStage;
    
    @FXML
    private Pane newCustomerButton;
    
    @FXML
    private Pane existingCustomerButton;
    
    @FXML
    private Label cancelButton;
    
    @FXML
    private void existingCustomerHandle(Event event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("ReservationCreate.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show(); 


    }
    
    @FXML
    private void cancelButtonHandle(Event event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("ReservationView.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show(); 
    
    }
    
    @FXML
    private void newButtonHandle(Event event) throws IOException{

        Parent root = FXMLLoader.load(getClass().getResource("PopUpCustomerCreate.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show(); 
        

    }
    
    void setStage(Stage oldStage){
        prevStage = oldStage;
    }
}
