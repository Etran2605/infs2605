/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application.database;

import Model.Guest;
import Model.Purchase;
import Model.Reservation;
import Model.StaffActivity;
import hotel.application.Purchases;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;

/**
 *
 * @author renazhou
 */
public class GuestQuery extends DatabaseQuery {

    PreparedStatement insertGuest = null;
    PreparedStatement updateGuest = null;
    PreparedStatement getAllGuests = null;
    PreparedStatement deleteGuest = null;
    PreparedStatement getAddress = null;
    PreparedStatement getEmail = null;
    PreparedStatement getLastName = null;
    PreparedStatement getFirstName = null;
    PreparedStatement getGuestID = null;
    PreparedStatement getPhone = null;
    PreparedStatement getFName = null;
    PreparedStatement getLName = null;

    ResultSet rs = null;

    /**
     * Retrieves a guest record from the guest table
     *
     */
    public List<Guest> getGuest() {

        List<Guest> guests = new ArrayList<>();
        openConnection();
        try {
            getAllGuests = conn.prepareStatement("select * from app.GUEST");
            rs = getAllGuests.executeQuery();
            while (rs.next()) {
                Guest newGuest = new Guest(rs.getInt("GUESTID"), rs.getString("firstname"), rs.getString("lastname"), rs.getString("email"), rs.getString("address"), rs.getString("phone"));
                System.out.println(newGuest);
                guests.add(newGuest);

            }
            rs.close();
            getAllGuests.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        closeConnection();

        return guests;
    }

    /**
     * Creates a guest record and inserts it into the guest table
     *
     */
    public void insertGuest(Guest g) {
        System.out.println("inserting guest entry");

        openConnection();
        try {
            insertGuest = conn.prepareStatement("insert into app.GUEST (firstname, lastname, email, address, phone) values (?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);//
            insertGuest.setString(1, g.getFname());
            insertGuest.setString(2, g.getLname());
            insertGuest.setString(3, g.getEmail());
            insertGuest.setString(4, g.getAddress());
            insertGuest.setString(5, g.getPhone());

            insertGuest.executeUpdate();

            ResultSet rs = insertGuest.getGeneratedKeys();
            rs.next();
            g.setGuestId(rs.getInt(1));

            if (rs != null) {
                rs.close();
            }
            insertGuest.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        closeConnection();
    }

    /**
     * Updates an existing guest record in the guest table
     *
     */
    public void updateGuest(Guest c) {

        System.out.println("Updating first name to: " + c.getFname());
        System.out.println("For id = " + c.getGuestID());
        openConnection();
        try {

            updateGuest = conn.prepareStatement("update app.GUEST set firstname=?, lastname=?, email=?, address=?, phone=? where guestID=?");

            updateGuest.setString(1, c.getFname());
            updateGuest.setString(2, c.getLname());
            updateGuest.setString(3, c.getEmail());
            updateGuest.setString(4, c.getAddress());
            updateGuest.setString(5, c.getPhone());
            updateGuest.setInt(6, c.getGuestID());

            updateGuest.executeUpdate();

            if (rs != null) {
                rs.close();
            }
            updateGuest.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        closeConnection();

        GuestQuery guestQuery = new GuestQuery();
        for (Guest c1 : guestQuery.getGuest()) {
            System.out.println(c1);
        }
    }

    public List<Integer> getGuestIDs() {
        List<Integer> guestIDs = new ArrayList<Integer>();
        for (Guest g : this.getGuest()) {
            guestIDs.add(g.getGuestID());
        }
        return guestIDs;
    }

    /**
     * Deletes a guest record from the guest table
     *
     */
    public void deleteGuest(Guest c) {
        System.out.println("deleting Guest");
        openConnection();
        try {

            deleteGuest = conn.prepareStatement("delete from app.GUEST where guestID = ?");
            deleteGuest.setInt(1, c.getGuestID());

            deleteGuest.executeUpdate();

            if (rs != null) {
                rs.close();
            }
            deleteGuest.close();

            StaffQuery sq = new StaffQuery();
            StaffActivityQuery saq = new StaffActivityQuery();
            DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
            saq.insertStaffActivity(new StaffActivity(sq.getUsername(), "DELETE CUSTOMER", df.format(new java.util.Date()), null,
                    new Integer(c.getGuestID()).toString()));

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        closeConnection();
    }

    
    public Integer getGuestID() {
        Integer guestID = null;
        openConnection();
        try {
            getGuestID = conn.prepareStatement("select guestid from app.GUEST");
            rs = getGuestID.executeQuery();
            while (rs.next()) {
                guestID = rs.getInt("guestid");
            }
            rs.close();
            getGuestID.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return guestID;

    }

    public String getFirstName() {
        String fname = null;
        openConnection();
        try {
            getFirstName = conn.prepareStatement("select firstname from app.GUEST");
            rs = getFirstName.executeQuery();
            while (rs.next()) {
                fname = rs.getString("firstname");
            }
            rs.close();
            getFirstName.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return fname;

    }

    public String getLastName() {
        String lname = null;
        openConnection();
        try {
            getLastName = conn.prepareStatement("select lastname from app.GUEST");
            rs = getLastName.executeQuery();
            while (rs.next()) {
                lname = rs.getString("lastname");
            }
            rs.close();
            getLastName.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return lname;

    }

    public String getEmail() {
        String email = null;
        openConnection();
        try {
            getEmail = conn.prepareStatement("select email from app.GUEST");
            rs = getEmail.executeQuery();
            while (rs.next()) {
                email = rs.getString("email");
            }
            rs.close();
            getEmail.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return email;

    }

    public String getAddress() {
        String address = null;
        openConnection();
        try {
            getAddress = conn.prepareStatement("select address from app.GUEST");
            rs = getAddress.executeQuery();
            while (rs.next()) {
                address = rs.getString("address");
            }
            rs.close();
            getAddress.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return address;

    }

    public String getPhone() {
        String phone = null;
        openConnection();
        try {
            getPhone = conn.prepareStatement("select phone from app.GUEST");
            rs = getPhone.executeQuery();
            while (rs.next()) {
                phone = rs.getString("phone");
            }
            rs.close();
            getPhone.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return phone;

    }

    public String getFName(int guestID) {
        System.out.println("Getting name " + guestID);
        String fname = null;
        openConnection();
        try {
            getFName = conn.prepareStatement("select firstname from app.GUEST WHERE guestID = ?");
            getFName.setInt(1, guestID);

            rs = getFName.executeQuery();
            if (rs.next()) {
                fname = rs.getString("fname");
            }
            rs.close();
            getFName.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        closeConnection();
        return fname;

    }

    public String getLName(int guestID) {
        System.out.println("Getting name " + guestID);
        String lname = null;
        openConnection();
        try {
            getLName = conn.prepareStatement("select lastname from app.GUEST WHERE guestID = ?");
            getLName.setInt(1, guestID);

            rs = getLName.executeQuery();
            if (rs.next()) {
                lname = rs.getString("lname");
            }
            rs.close();
            getLName.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        closeConnection();
        return lname;

    }
    
}
