/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application.database;

import Model.Purchase;
import Model.Reservation;
import Model.Room;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import static javafx.application.Application.launch;

import Model.StaffActivity;
import javafx.event.ActionEvent;

/**
 * @author renazhou
 */
public class ReservationQuery extends DatabaseQuery {

    private static Object reservation;
    PreparedStatement insertReservation = null;
    PreparedStatement updateReservation = null;
    PreparedStatement deleteReservation = null;
    PreparedStatement getAllReservation = null;
    PreparedStatement getAllBookedRooms = null;
    PreparedStatement getBookingID = null;
    PreparedStatement getEarlyCheckInAvailability = null;
    PreparedStatement getResCost = null;
    PreparedStatement getDepositPaid = null;
    PreparedStatement getFreeBreakfast = null;
    PreparedStatement getAdjustedDate = null;
    PreparedStatement getCheckOut = null;
    PreparedStatement getCheckIn = null;
    PreparedStatement getRoomNumber = null;
    PreparedStatement getNumPeople = null;
    PreparedStatement getGuestID = null;
    PreparedStatement getRefCode = null;

    ResultSet rs = null;

    /**
     * Finds a reservation given a booking ID
     *
     * @param id the booking id which is used to get a reservation
     *
     * @returns null
     */
    public Reservation getReservationById(int id) {
        System.out.println("Getting reservation " + id);
        List<Reservation> reservations = new ArrayList<>();
        openConnection();
        try {
            getAllReservation = conn.prepareStatement("select * from app.RESERVATION WHERE id = ?");
            getAllReservation.setInt(1, id);

            rs = getAllReservation.executeQuery();
            if (rs.next()) {
                return new Reservation(rs.getInt("ID"), rs.getString("refCode"), rs.getInt("guestID"), rs.getString("numPeople"), rs.getString("roomNumber"), rs.getDate("checkIn"), rs.getDate("checkOut"), rs.getString("adjustedCheck"), rs.getString("breakfastdates"), rs.getInt("deposit"), rs.getInt("cost"));
            }
            rs.close();
            getAllReservation.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        closeConnection();
        return null;
    }

    /**
     * Retrieves a reservation from the reservation table 
     *
     */
    public List<Reservation> getReservation() {
        System.out.println("Getting reservation");
        List<Reservation> reservations = new ArrayList<>();
        openConnection();
        try {
            getAllReservation = conn.prepareStatement("select * from app.RESERVATION");
            rs = getAllReservation.executeQuery();
            while (rs.next()) {
                reservations.add(
                        new Reservation(rs.getInt("ID"), rs.getString("refCode"), rs.getInt("guestID"), rs.getString("numPeople"), rs.getString("roomNumber"), rs.getDate("checkIn"), rs.getDate("checkOut"), rs.getString("adjustedCheck"), rs.getString("breakfastdates"), rs.getInt("deposit"), rs.getInt("cost"))
                );
            }
            rs.close();
            getAllReservation.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        closeConnection();
        System.out.println("returning " + reservations.size() + " reservations");
        return reservations;
    }

    /**
     * Creates a reservation record and inserts it to the reservation table
     */
    public void insertReservation(Reservation r) {
        System.out.println("inserting res");

        openConnection();
        try {

            insertReservation = conn.prepareStatement("insert into app.RESERVATION (refCode, guestID, numPeople, roomNumber, checkIn, checkOut, adjustedCheck, breakfastDates, deposit, cost, owing) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);//
            insertReservation.setString(1, r.getRefCode());
            insertReservation.setInt(2, r.getGuestID());
            insertReservation.setString(3, r.getNumPeople());
            insertReservation.setString(4, r.getRoomNumber());
            insertReservation.setDate(5, r.getCheckIn());
            insertReservation.setDate(6, r.getCheckOut());
            insertReservation.setString(7, r.getAdjustedCheck());
            insertReservation.setString(8, r.getFreeBreakfast());
            insertReservation.setInt(9, r.getDepositPaid());
            insertReservation.setInt(10, r.getResCost());
            insertReservation.setInt(11, r.getAmountOwing());

            insertReservation.executeUpdate();

            ResultSet rs = insertReservation.getGeneratedKeys();
            rs.next();
            r.setId(rs.getInt(1));
            System.out.println("Resrvation id = " + r.getID());

            if (rs != null) {
                rs.close();
            }
            insertReservation.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        closeConnection();
    }

    /**
     * Updates existing reservation records in the reservation table
     */
    public void updateReservation(Reservation r) {

        System.out.println("Updating room number to: " + r.getRoomNumber());
        System.out.println("For id = " + r.getID());
        openConnection();
        try {

            updateReservation = conn.prepareStatement("update app.RESERVATION set guestID=?, numPeople=?, roomNumber=?, checkIn=?, checkOut=?, adjustedCheck=?, breakfastDates=?, deposit=?, cost=? where id=?");
            updateReservation.setInt(1, r.getGuestID());
            updateReservation.setString(2, r.getNumPeople());
            updateReservation.setString(3, r.getRoomNumber());
            updateReservation.setDate(4, r.getCheckIn());
            updateReservation.setDate(5, r.getCheckOut());
            updateReservation.setString(6, r.getAdjustedCheck());
            updateReservation.setString(7, r.getFreeBreakfast());
            updateReservation.setInt(8, r.getDepositPaid());
            updateReservation.setInt(9, r.getResCost());
            updateReservation.setInt(10, r.getID());

            updateReservation.executeUpdate();

            if (rs != null) {
                rs.close();
            }
            updateReservation.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        closeConnection();

        ReservationQuery reservationQuery = new ReservationQuery();
        for (Reservation r1 : reservationQuery.getReservation()) {
            System.out.println(r1);
        }
    }

    /**
     * Deletes existing reservation records in the reservation table
     */
    public void deleteReservation(Reservation r) {

        openConnection();
        try {

            deleteReservation = conn.prepareStatement("delete from app.RESERVATION where id = ?");
            deleteReservation.setInt(1, r.getID());

            deleteReservation.executeUpdate();

            if (rs != null) {
                rs.close();
            }
            deleteReservation.close();

            StaffQuery sq = new StaffQuery();
            StaffActivityQuery saq = new StaffActivityQuery();
            DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
            saq.insertStaffActivity(new StaffActivity(sq.getUsername(), "DELETE RESERVATION", df.format(new java.util.Date()),
                    new Integer(r.getID()).toString(), null));

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        closeConnection();
    }

    /**
     * Retrieves all the booked rooms on and between a given start and end date
     *
     * @param startDate the given check in date for a reservation to determine
     * whether the room is booke
     * @param endDate the given check out date for a reservation to determine
     * whether the room is booked
     *
     * @return the room numbers of the booked rooms
     *
     */
    public List<String> getBookedRooms(Date startDate, Date endDate) {
        List<String> roomNumbers = new ArrayList<>();
        openConnection();
        try {
            getAllBookedRooms = conn.prepareStatement("select DISTINCT roomNumber from app.RESERVATION where "
                    + "(? >= checkIn AND ? <= checkOut) OR"
                    + "(? < checkIn AND ? < checkOut AND ? > checkIn) OR"
                    + "(? > checkIn AND ? < checkOut AND ? > checkOut) OR"
                    + "(? < checkIn and ? > checkOut)"
            );
            getAllBookedRooms.setDate(1, startDate);
            getAllBookedRooms.setDate(2, endDate);
            getAllBookedRooms.setDate(3, startDate);
            getAllBookedRooms.setDate(4, endDate);
            getAllBookedRooms.setDate(5, endDate);
            getAllBookedRooms.setDate(6, startDate);
            getAllBookedRooms.setDate(7, startDate);
            getAllBookedRooms.setDate(8, endDate);
            getAllBookedRooms.setDate(9, startDate);
            getAllBookedRooms.setDate(10, endDate);

            rs = getAllBookedRooms.executeQuery();
            while (rs.next()) {
                roomNumbers.add(
                        rs.getString("roomNumber")
                );
            }
            rs.close();
            getAllBookedRooms.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        closeConnection();
        return roomNumbers;

    }

    /**
     * Checks whether a room is available in the morning depending on whether
     * another booking has been made to check out on the same date
     *
     * @param startDate the date which is to be checked for whether the room is
     * available
     * @param roomnum the room number
     *
     * @return available, a boolean value which determines whether that room is
     * available on that morning
     */
    public boolean isRoomAvailableThatMorning(Date startDate, Integer roomnum) {

        boolean available = true;
        openConnection();
        try {
            getAllReservation = conn.prepareStatement("select * from app.RESERVATION where "
                    + "checkOut = ? AND "
                    + " roomnumber =  ?");
            getAllReservation.setDate(1, startDate);
            getAllReservation.setInt(2, roomnum);
            rs = getAllReservation.executeQuery();
            if (rs.next()) {
                available = false;
            }
            rs.close();
            getAllReservation.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        closeConnection();

        return available;

    }

    /**
     * Checks whether a room is available in the afternoon depending on whether
     * another booking has been made to check in on the same date
     *
     * @param endDate the date which is to be checked for whether the room is
     * available
     * @param roomnum the room number
     *
     * @return available, a boolean value which determines whether that room is
     * available on that morning
     */
    public boolean isRoomAvailableThatAfternoon(Date endDate, Integer roomnum) {
        boolean available = true;
        openConnection();
        try {
            getAllReservation = conn.prepareStatement("select * from app.RESERVATION where "
                    + "checkIn = ? AND "
                    + " roomnumber =  ?");
            getAllReservation.setDate(1, endDate);
            getAllReservation.setInt(2, roomnum);
            rs = getAllReservation.executeQuery();
            rs = getAllReservation.executeQuery();
            if (rs.next()) {
                available = false;
            }
            rs.close();
            getAllReservation.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        closeConnection();

        return available;
    }

    public List<String> getRefCodes() {
        List<String> refCodes = new ArrayList<String>();
        for (Reservation r : this.getReservation()) {
            refCodes.add(r.getRefCode());
        }
        return refCodes;
    }

    /**
     * Converts a LocalDate object into a java.util.Date object of the same
     * date.
     *
     * @param localDate The local date to convert into a java.util.Date object
     * @return The equivalent value in java.util.Date form
     */
    private java.util.Date localDateToUtilDate(LocalDate startDate) {

        List<String> breakfastDates = new ArrayList<String>();
        ReservationQuery resQ = new ReservationQuery();

        GregorianCalendar cal = new GregorianCalendar(
                startDate.getYear(), startDate.getMonthValue() - 1, startDate.getDayOfMonth());
        java.util.Date date = cal.getTime();
        return date;

    }

    public String[] getDays() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Integer getBookingID() {
        Integer bookingID = null;
        openConnection();
        try {
            getBookingID = conn.prepareStatement("select ID from app.RESERVATION");
            rs = getBookingID.executeQuery();
            while (rs.next()) {
                bookingID = rs.getInt("ID");
            }
            rs.close();
            getBookingID.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return bookingID;

    }

    public String getRefCode() {
        String refCode = null;
        openConnection();
        try {
            getRefCode = conn.prepareStatement("select refcode from app.RESERVATION");
            rs = getRefCode.executeQuery();
            while (rs.next()) {
                refCode = rs.getString("refCode");
            }
            rs.close();
            getRefCode.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return refCode;

    }

    public int getGuestID() {
        Integer guestID = null;
        openConnection();
        try {
            getGuestID = conn.prepareStatement("select guestid from app.RESERVATION");
            rs = getGuestID.executeQuery();
            while (rs.next()) {
                guestID = rs.getInt("guestID");
            }
            rs.close();
            getGuestID.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return guestID;

    }

    public String getNumPeople() {
        String numPeople = null;
        openConnection();
        try {
            getNumPeople = conn.prepareStatement("select numpeople from app.RESERVATION");
            rs = getNumPeople.executeQuery();
            while (rs.next()) {
                numPeople = rs.getString("numPeople");
            }
            rs.close();
            getNumPeople.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return numPeople;

    }

    public String getRoomNumber() {
        String roomNumber = null;
        openConnection();
        try {
            getRoomNumber = conn.prepareStatement("select roomnumber from app.RESERVATION");
            rs = getRoomNumber.executeQuery();
            while (rs.next()) {
                roomNumber = rs.getString("roomNumber");
            }
            rs.close();
            getRoomNumber.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return roomNumber;

    }

    public Date getCheckIn() {
        Date checkIn = null;
        openConnection();
        try {
            getCheckIn = conn.prepareStatement("select checkin from app.RESERVATION");
            rs = getCheckIn.executeQuery();
            while (rs.next()) {
                checkIn = rs.getDate("checkIn");
            }
            rs.close();
            getCheckIn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return checkIn;

    }

    public Date getCheckOut() {
        Date checkOut = null;
        openConnection();
        try {
            getCheckOut = conn.prepareStatement("select checkout from app.RESERVATION");
            rs = getCheckOut.executeQuery();
            while (rs.next()) {
                checkOut = rs.getDate("checkOut");
            }
            rs.close();
            getCheckOut.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return checkOut;

    }

    public String getAdjustedDate() {
        String adjustedDate = null;
        openConnection();
        try {
            getAdjustedDate = conn.prepareStatement("select adjustedcheck from app.RESERVATION");
            rs = getAdjustedDate.executeQuery();
            while (rs.next()) {
                adjustedDate = rs.getString("adjustedcheck");
            }
            rs.close();
            getAdjustedDate.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return adjustedDate;

    }

    public String getFreeBreakfast() {
        String freeBreakfast = null;
        openConnection();
        try {
            getFreeBreakfast = conn.prepareStatement("select BREAKFASTDATES from app.RESERVATION");
            rs = getFreeBreakfast.executeQuery();
            while (rs.next()) {
                freeBreakfast = rs.getString("breakfastdates");
            }
            rs.close();
            getFreeBreakfast.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return freeBreakfast;

    }

    public Integer getDepositPaid() {
        Integer depositPaid = null;
        openConnection();
        try {
            getDepositPaid = conn.prepareStatement("select deposit from app.RESERVATION");
            rs = getDepositPaid.executeQuery();
            while (rs.next()) {
                depositPaid = rs.getInt("deposit");
            }
            rs.close();
            getDepositPaid.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return depositPaid;

    }

    public Integer getResCost() {
        Integer resCost = null;
        openConnection();
        try {
            getResCost = conn.prepareStatement("select cost from app.RESERVATION");
            rs = getResCost.executeQuery();
            while (rs.next()) {
                resCost = rs.getInt("cost");
            }
            rs.close();
            getResCost.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return resCost;

    }
}
