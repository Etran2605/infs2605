/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application.database;

import Model.StaffActivity;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tranny
 */
public class StaffActivityQuery extends DatabaseQuery{

     PreparedStatement insertGuest = null;
     PreparedStatement getAllActivity = null;
     PreparedStatement insertStaffActivity = null;
     
     ResultSet rs = null;
     
     /**
     * Retrieves the activity type from the staff log table
     *
     */
     public List<StaffActivity> getActivity(){
         System.out.println("Getting Activity List");
         List<StaffActivity> staffActivity = new ArrayList<>();
         openConnection();
         try {
             getAllActivity = conn.prepareStatement("select * from app.STAFFLOG");
             rs = getAllActivity.executeQuery();
             while(rs.next()) {
                 staffActivity.add(
                         new StaffActivity(rs.getInt("activityID"),  rs.getString("username"), rs.getString("actionType"), rs.getString("date"), rs.getString("bookingID"), rs.getString("guestID"))
                    );
             }
             
            rs.close();
            getAllActivity.close();
            

         } catch (SQLException ex) {
             ex.printStackTrace();
         }
         closeConnection();
         System.out.println("returning " + staffActivity.size() + " Staff Activities");
         return staffActivity;
     }
     
         /**
     * Creates a staff activity log record and inserts it into the staff log table
     *
     */
     public void insertStaffActivity(StaffActivity s){
         System.out.println("inserting staff activity log");
         System.out.println("In");
         
         openConnection();
         try {
             
             insertStaffActivity = conn.prepareStatement("insert into app.STAFFLOG (username, actionType, date, bookingid, guestid) values (?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
             insertStaffActivity.setString(1,s.getName());
             insertStaffActivity.setString(2, s.getActivityType());
             insertStaffActivity.setString(3, s.getDate());
             insertStaffActivity.setString(4, s.getBookingID());
             insertStaffActivity.setString(5, s.getGuestID());
             insertStaffActivity.executeUpdate();

             ResultSet rs = insertStaffActivity.getGeneratedKeys();
             rs.next();
             s.setActivityID(rs.getInt(1));
            
             if (rs != null) {
             rs.close();
            }
            insertStaffActivity.close();
         
         } catch (SQLException ex) {
             ex.printStackTrace();
         }
         
         closeConnection();
     }
     
}
