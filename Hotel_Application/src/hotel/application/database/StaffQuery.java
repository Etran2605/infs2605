/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application.database;

import Model.Reservation;
import Model.Staff;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author renazhou
 */
public class StaffQuery extends DatabaseQuery {

    ResultSet rs = null;

    PreparedStatement getAllStaff = null;
    PreparedStatement getStaffUsername = null;

    /**
     * The method run to allow or deny access to the system
     *
     * @param username the username entered
     * @param password the password entered
     *
     * @return whether the user gains access to the system
     *
     */
    public boolean login(String username, String password) {
        boolean access = false;

        openConnection();

        try {

            Statement stmt = conn.createStatement();
            String loginQuery = "SELECT * FROM APP.STAFF WHERE login = '" + username + "' AND password = '" + password + "' ";
            System.out.println(loginQuery);
            ResultSet rs = stmt.executeQuery(loginQuery);

            if (rs.next()) {

                access = true;
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            closeConnection();
        }

        return access;
    }

    /**
     * Creates and inserts a staff record into the staff table
     *
     */
    public void insertStaff(Staff s) {

        PreparedStatement insertStaff = null;
        PreparedStatement getStaff = null;

        ResultSet rs = null;

        openConnection();
        try {

            insertStaff = conn.prepareStatement("insert into app.STAFF (login, password) values (?, ?)", Statement.RETURN_GENERATED_KEYS);//
            insertStaff.setString(1, s.getUsername());
            insertStaff.setString(2, s.getPassword());

            insertStaff.executeUpdate();

            if (rs != null) {
                rs.close();
            }
            insertStaff.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        closeConnection();
    }

    /**
     * Retrieves a staff record from the staff table
     *
     */
    public List<Staff> getStaff() {

        List<Staff> staff = new ArrayList<>();
        openConnection();
        try {
            getAllStaff = conn.prepareStatement("select * from app.STAFF");
            rs = getAllStaff.executeQuery();
            while (rs.next()) {
                staff.add(
                        new Staff(rs.getInt("ID"), rs.getString("LOGIN"), rs.getString("PASSWORD"))
                );
            }
            rs.close();
            getAllStaff.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        closeConnection();
        System.out.println("returning " + staff.size() + " staff");
        return staff;
    }

    public String getUsername() {
        String username = null;
        openConnection();
        try {
            getStaffUsername = conn.prepareStatement("select login from app.STAFF");
            rs = getStaffUsername.executeQuery();
            while (rs.next()) {
                username = rs.getString("login");
            }
            rs.close();
            getStaffUsername.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return username;
    }

}
