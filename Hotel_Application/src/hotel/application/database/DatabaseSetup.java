/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application.database;

import Model.Staff;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author renazhou
 */
public class DatabaseSetup extends DatabaseQuery {

    PreparedStatement createGuestTable = null;
    PreparedStatement createPurchasesTable = null;
    PreparedStatement createStaffLogTable = null;
    PreparedStatement createStaffTable = null;
    PreparedStatement createReservationTable = null;
    PreparedStatement createRoomTable = null;

    ResultSet rs = null;

    public static void setupDatabase() {
        DatabaseSetup dbs = new DatabaseSetup();
        dbs.databaseSetup();
    }

    /**
     * Sets up the database and begins by checking whether a reservation table
     * already exists.
     */
    private void databaseSetup() {

        openConnection();

        try {

            DatabaseMetaData dbmd = conn.getMetaData();
            rs = dbmd.getTables(null, "APP", "RESERVATION", null);

            if (!rs.next()) {
                // If the RESERVATION table does not already exist we create it
                createReservationTable = conn.prepareStatement(
                        "CREATE TABLE APP.RESERVATION ("
                        + "\"ID\" INT not null primary key "
                        + "GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
                        + "\"REFCODE\" VARCHAR(50),"
                        + "\"GUESTID\" VARCHAR(50),"
                        + "\"NUMPEOPLE\" VARCHAR(50),"
                        + "\"ROOMNUMBER\" VARCHAR(50),"
                        + "\"CHECKIN\" VARCHAR(50),"
                        + "\"CHECKOUT\" VARCHAR(50),"
                        + "\"ADJUSTEDCHECK\" VARCHAR(50),"
                        + "\"BREAKFASTDATES\" VARCHAR(50),"
                        + "\"DEPOSIT\" INTEGER,"
                        + "\"COST\" INTEGER,"
                        + "\"OWING\" INTEGER)"
                );
                createReservationTable.execute();
                createStaffTable = conn.prepareStatement(
                        "CREATE TABLE APP.STAFF ("
                        + "\"ID\" INT not null primary key "
                        + "GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
                        + "\"LOGIN\" VARCHAR(100),"
                        + "\"PASSWORD\" VARCHAR(100))"
                );
                createStaffTable.execute();
                createPurchasesTable = conn.prepareStatement(
                        "CREATE TABLE APP.PURCHASES ("
                        + "\"ID\" INT not null primary key "
                        + "GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
                        + "\"RESID\" INTEGER REFERENCES APP.RESERVATION(ID), "
                        + "\"PURCHASEQUANTITY\" VARCHAR(100),"
                        + "\"PURCHASEDESC\" VARCHAR(100),"
                        + "\"PURCHASECOST\" VARCHAR(100))"
                );
                createPurchasesTable.execute();
                createStaffLogTable = conn.prepareStatement(
                        "CREATE TABLE APP.STAFFLOG ("
                        + "\"ACTIVITYID\" INT not null primary key "
                        + "GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
                        + "\"USERNAME\" VARCHAR(50),"
                        + "\"ACTIONTYPE\" VARCHAR(50),"
                        + "\"DATE\" VARCHAR(50),"
                        + "\"BOOKINGID\" VARCHAR(50),"
                        + "\"GUESTID\" VARCHAR(50))"
                );
                createStaffLogTable.execute();
                createGuestTable = conn.prepareStatement(
                        "CREATE TABLE APP.GUEST ("
                        + "\"GUESTID\" INT not null primary key "
                        + "GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
                        + "\"FIRSTNAME\" VARCHAR(100),"
                        + "\"LASTNAME\" VARCHAR(100),"
                        + "\"EMAIL\" VARCHAR(100),"
                        + "\"ADDRESS\" VARCHAR(100),"
                        + "\"PHONE\" VARCHAR(100))"
                );
                createGuestTable.execute();

                StaffQuery sq = new StaffQuery();
                sq.insertStaff(new Staff("admin", "password"));

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        closeConnection();
    }

}
