/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application.database;

import Model.Reservation;
import Model.Room;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author renazhou
 */
public class RoomViewQuery extends DatabaseQuery {

    /**
     * Reads in the room records from a csv file
     *
     */
    public List<Room> getRoomsFromFile() {
        List<Room> rooms = new ArrayList<Room>();
        try {
            Scanner scanner = new Scanner(new File("resources/rooms.csv"));

            while (scanner.hasNext()) {
                String r = scanner.nextLine();
                String[] terms = r.split(",");

                Room newRoom = new Room(
                        terms[0], // room num
                        terms[1], // name
                        terms[2], // description
                        Integer.parseInt(terms[3])); // cost per night

                rooms.add(newRoom);
            }

            // Close the file
            scanner.close();

        } catch (FileNotFoundException ex) {
        }
        return rooms;
    }

        /**
     * Gets the room numbers from our csv file which stores all the hotel rooms. This is used for the combobox to allow the user to select what room they would like to book for the reservation
     *
     * @return the room number to display in the combobox
     */
    public List<String> getRoomNums() {
        List<String> roomNums = new ArrayList<>();
        for (Room rm : this.getRoomsFromFile()) {
            roomNums.add(rm.getNumber().getValue());
        }
        return roomNums;

    }

}
