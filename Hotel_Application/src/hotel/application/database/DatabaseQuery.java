/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author renazhou
 */
public class DatabaseQuery {

    protected Connection conn;
    
    /**
     * Opens the connection to the database
     */
    protected void openConnection()  {
        if (conn == null) {
            try {
             
                //connect to the database
                conn = DriverManager.getConnection("jdbc:derby:"
                        + System.getProperty("user.dir")
                        + System.getProperty("file.separator")
                        + "hotelDB;create=true");
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

     /**
     * Closes the connection to the database
     */
    protected void closeConnection() {
        try {
            if (conn != null) {
                conn.close();
                conn = null;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}

