/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application;

import Model.Purchase;
import Model.Reservation;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.ZapfDingbatsList;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfWriter;
import hotel.application.database.DatabaseSetup;
import hotel.application.database.GuestQuery;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import static javafx.application.Application.launch;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import com.itextpdf.text.pdf.PdfPTable;
import hotel.application.database.PurchasesQuery;
import hotel.application.database.ReservationQuery;
import java.awt.Desktop;
import java.text.DecimalFormat;
import javafx.scene.control.ComboBox;


/**
 * FXML Controller class
 *
 * @author Tranny
 */
public class PurchasesController implements Initializable {

    @FXML
    private ComboBox RefCodeComboBox;

    @FXML
    private TextArea itemDescriptionTextArea;

    @FXML
    private TextField itemCostTextField;

    @FXML
    private TextField quantityTextField;

    @FXML
    private ListView purchasesListView;

    @FXML
    private Button addPurchasesButton;

    @FXML
    private Button printButton;

    @FXML
    private Button cancelButton;

    private PurchasesQuery purchasesQuery = new PurchasesQuery();

    private ObservableList<Purchase> purchasesList;
    
    


    @FXML 
    private void onReservationChange(ActionEvent event) {
        purchasesList = FXCollections.observableArrayList(purchasesQuery.getPurchases((String)RefCodeComboBox.getValue()));
        purchasesListView.setItems(purchasesList);
    }
    
    @FXML
    private void addPurchaseButton(ActionEvent event) {
        int newPurchaseQuantity = Integer.parseInt(quantityTextField.getText());
        String newPurchaseDesc = itemDescriptionTextArea.getText();
        Double newPurchaseCost = Double.parseDouble(itemCostTextField.getText());

        Purchase newPurchase = new Purchase(newPurchaseQuantity, 
                new Integer((String)RefCodeComboBox.getValue()), 
                        newPurchaseDesc, newPurchaseCost);
        System.out.println("purchasesQuery = " + purchasesQuery);
        System.out.println("newPurchase = " + newPurchase);
        purchasesQuery.insertPurchase(newPurchase);
        purchasesList.add(newPurchase);


    }

        /**
     * The print button event handler allows for the purchases/ invoice page to pop up as a pdf
     *
     */
    @FXML
    private void printButton(ActionEvent event) throws BadElementException, IOException {
        try {
            DecimalFormat df = new DecimalFormat("#.0");
                  
            Integer selectedResID = Integer.parseInt((String) RefCodeComboBox.getSelectionModel().getSelectedItem());
            ReservationQuery rq = new ReservationQuery();
            Reservation r = rq.getReservationById(selectedResID);

            Document document = new Document();

            FileOutputStream output = null;

            output = new FileOutputStream("myPDF.pdf");

            PdfWriter.getInstance(document, output);
            
            Font font1 = new Font(Font.FontFamily.HELVETICA  , 18, Font.BOLD);
            Font font2 = new Font(Font.FontFamily.COURIER    , 30,
            Font.BOLD | Font.UNDERLINE);
            Font font3 = new Font(Font.FontFamily.HELVETICA, 10);
            Font font4 = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
            Font font5 = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
            Font font6 = new Font(Font.FontFamily.HELVETICA, 24,
            Font.ITALIC);

            document.open();

            PdfPTable table = new PdfPTable(3);

            PdfPCell cell1 = new PdfPCell(new Paragraph("Quantity", font4));
            PdfPCell cell2 = new PdfPCell(new Paragraph("Purchase Item", font4));
            PdfPCell cell3 = new PdfPCell(new Paragraph("Purchase Cost ($)", font4));
            table.addCell(cell1);
            table.addCell(cell2);
            table.addCell(cell3);

            Double cost = 0.0;
            for (Purchase p : purchasesList) {
                PdfPCell cell4 = new PdfPCell(new Paragraph("" + p.getPurchaseQuantity()));
                PdfPCell cell5 = new PdfPCell(new Paragraph(p.getPurchaseDesc()));
                PdfPCell cell6 = new PdfPCell(new Paragraph(p.getPurchaseCost().toString() +"0"));
                table.addCell(cell4);
                table.addCell(cell5);
                table.addCell(cell6);
                cost += p.getPurchaseCost() * p.getPurchaseQuantity();
            }
            Double totalCost = 0.0;
            totalCost = cost + r.getAmountOwing();
            
            document.add(new Paragraph("CHECK OUT INVOICE", font2));
            document.add(new Paragraph (" "));
            document.add(new Paragraph (" "));
            document.add(new Paragraph("Guest ID: " +r.getGuestID(), font3));       
            document.add(new Paragraph("Booking ID: " +r.getID(), font3));  
            document.add(new Paragraph("Room Number: " +r.getRoomNumber(), font3));
            document.add(new Paragraph("Check In Date: " +r.getCheckIn(), font3));
            document.add(new Paragraph("Check Out Date: " +r.getCheckOut(), font3));
            document.add(new Paragraph (" "));
            document.add(new Paragraph("Your purchases are displayed below: "));
            document.add(new Paragraph (" "));
            document.add(table);
            document.add(new Paragraph (" "));
            document.add(new Paragraph("The total cost of your purchases is: $" +cost + "0", font3));
            document.add(new Paragraph("Amount owing for reservation: $" + r.getAmountOwing() + ".00", font3));
            document.add(new Paragraph("Total Amount Owing: $" +totalCost +"0", font5));
            document.add(new Paragraph (" "));
            document.add(new Paragraph (" "));
            document.add(new Paragraph (" "));
            document.add(new Paragraph("           Thank you for staying at City Resort! ", font6));
            

            Image image = Image.getInstance("resources/hotellogo.png");
               
            image.setAbsolutePosition(445, 700);
            document.add(new Paragraph (" "));
            document.add(new Paragraph (" "));
            document.add(new Paragraph (" "));
            document.add(new Paragraph (" "));
            document.add(new Paragraph (" "));
            document.add(new Paragraph (" "));
            document.add(new Paragraph (" "));
            document.add(new Paragraph (" "));
            document.add(new Paragraph (" "));
            document.add(new Paragraph (" "));
            document.add(new Paragraph (" "));
            document.add(new Paragraph (" "));
            document.add(new Paragraph (" "));
            document.add(new Paragraph (" "));
            document.add(new Paragraph (" "));
            image.scalePercent(20);
            document.add(image);


            document.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PurchasesController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(PurchasesController.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (Desktop.isDesktopSupported()) {
            try {
                java.io.File myFile = new java.io.File("myPDF.pdf");
                Desktop.getDesktop().open(myFile);  
            } catch (IOException e){
                e.printStackTrace();
            }

        }
    }

    @FXML
    private void handleCancelButton(ActionEvent event) throws IOException {

        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();

    }


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        //comboBox values
        ObservableList<String> refCodeOptions = FXCollections.observableArrayList();

        ReservationQuery rq = new ReservationQuery();
        List<Reservation> rs = rq.getReservation();
        for (Reservation r: rs) {
            refCodeOptions.add(new Integer(r.getID()).toString());
        }

        RefCodeComboBox.setItems(refCodeOptions);

    }

}