/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application;

import Model.Guest;
import Model.Staff;
import Model.StaffActivity;
import hotel.application.database.GuestQuery;
import hotel.application.database.StaffActivityQuery;
import hotel.application.database.StaffQuery;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Tranny
 */
public class CustomerToReservationController implements Initializable {

    
    @FXML
    private TextField guestIDTextfield;
    
    @FXML
    private TextField fNameTextfield;

    @FXML
    private TextField staffRefCode;
    
    @FXML
    private TextField lNameTextfield;
    
    @FXML
    private TextField emailAddTextfield;
    
    @FXML
    private TextArea postalAddTextArea;
    
    @FXML
    private TextField contactNumberTextfield;
    
    @FXML
    private Button okButton;
    
    GuestQuery guestQuery = new GuestQuery();
    private final ObservableList<Guest> guestData = FXCollections.observableArrayList();  
    
    StaffQuery staffQuery = new StaffQuery();
    Staff staff;
    private ObservableList<Staff> staffData = FXCollections.observableArrayList();

     StaffActivityQuery staffActivityQuery = new StaffActivityQuery();
     StaffActivity staffActivity;
     private ObservableList<StaffActivity> staffActivityData = FXCollections.observableArrayList();
     
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        staffRefCode.setText(staffQuery.getUsername());
        guestIDTextfield.setText("" + guestQuery.getGuestID());
        fNameTextfield.setText(guestQuery.getFirstName());
        
        lNameTextfield.setText(guestQuery.getLastName());
        emailAddTextfield.setText(guestQuery.getEmail());
        postalAddTextArea.setText(guestQuery.getAddress());
        contactNumberTextfield.setText(guestQuery.getPhone());

    }    

    private void createActivityRecord() {
        
        DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        Date dateobj = new Date();
        Calendar calobj = Calendar.getInstance();
         
       

        String date = ("" + df.format(dateobj) + df.format(calobj.getTime()));
        String bookingID = null;
        String username = staffRefCode.getText();
        String activityType = "CREATE CUSTOMER";
        String guestID = guestIDTextfield.getText();



        StaffActivity entry = new StaffActivity(username, activityType, date, bookingID, guestID);

        staffActivityData.add(entry);
        staffActivityQuery.insertStaffActivity(entry);

     }     
    
    @FXML
    private void okButtonHandle(ActionEvent event) throws IOException{
                
        createActivityRecord();
 
         Parent root = FXMLLoader.load(getClass().getResource("ReservationCreate.fxml"));
         Scene scene = new Scene(root);
         Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
         app_stage.hide();
         app_stage.setScene(scene);
 
         app_stage.show(); 
    }
}
