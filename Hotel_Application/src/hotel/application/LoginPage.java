/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application;

import hotel.application.database.DatabaseSetup;
import java.io.IOException;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author renazhou
 */
public class LoginPage extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("LoginPage.fxml"));

        Scene scene = new Scene(root);
        stage.setTitle("Login Page");
        stage.setScene(scene);
        stage.show();
        stage.setResizable(false);
    }

        /**
     * Opens the Dash Board page
     */
    public void openDashBoard() {
          
        Dashboard dash = new Dashboard();
        Stage stage = new Stage();

        stage.initModality(Modality.APPLICATION_MODAL);
    }

    public void wrongPassword() {
        Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.WINDOW_MODAL);


        Button ok = new Button("OK");
        ok.setDefaultButton(true);

        ok.setOnMouseClicked(new EventHandler<MouseEvent>(){

            @Override
            public void handle(MouseEvent event) {
                dialogStage.close();
            }
        
        });
        

        dialogStage.setScene(new Scene(VBoxBuilder.create().
                children(new Text("Wrong username or password. Please Try Again."), ok).
                alignment(Pos.CENTER).padding(new Insets(5)).build()));
        dialogStage.show();
    }

    

}
