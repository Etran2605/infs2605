/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application;

import Model.Staff;
import Model.StaffActivity;
import hotel.application.database.StaffActivityQuery;
import hotel.application.database.StaffQuery;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Tranny
 */
public class StaffActivityLogController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        activityIDTableColumn.setCellValueFactory(new PropertyValueFactory<>("activityID"));
        staffRefTableColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        activityTypeTableColumn.setCellValueFactory(new PropertyValueFactory<>("activityType"));
        timeTableColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
        refCodeTableColumn.setCellValueFactory(new PropertyValueFactory<>("bookingID"));
        guestIDTableColumn.setCellValueFactory(new PropertyValueFactory<>("guestID"));

        staffActivityData.addAll(staffActivityQuery.getActivity());
        staffActivityTableView.setItems(staffActivityData);

        loggedInUser.setText(staffQuery.getUsername());
    }
    StaffQuery staffQuery = new StaffQuery();
    Staff staff;
    private ObservableList<Staff> staffData = FXCollections.observableArrayList();

    @FXML
    private Label loggedInUser;

    @FXML
    private TableView staffActivityTableView;

    @FXML
    private TableColumn activityIDTableColumn;

    @FXML
    private TableColumn staffRefTableColumn;

    @FXML
    private TableColumn activityTypeTableColumn;

    @FXML
    private TableColumn timeTableColumn;

    @FXML
    private TableColumn refCodeTableColumn;

    @FXML
    private TableColumn guestIDTableColumn;

    @FXML
    private Pane dashboardPaneButton;

    @FXML
    private Label dashboardLabelButton;

    StaffActivityQuery staffActivityQuery = new StaffActivityQuery();
    StaffActivity staffActivity;

    private ObservableList<StaffActivity> staffActivityData = FXCollections.observableArrayList();

    @FXML
    private void dashboardHandle(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("Dashboard.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();
    }

}
