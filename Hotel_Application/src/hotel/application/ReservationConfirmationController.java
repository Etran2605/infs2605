/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application;

import Model.Reservation;
import Model.StaffActivity;
import hotel.application.database.ReservationQuery;
import hotel.application.database.StaffActivityQuery;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Tranny
 */
public class ReservationConfirmationController implements Initializable {

    @FXML
    private TextField refCodeTextField;

    @FXML
    private ComboBox roomNumberComboBox;

    @FXML
    private ComboBox guestIDComboBox;

    @FXML
    private TextField numGuestTextField;

    @FXML
    private ComboBox specialCheckInComboBox;

    @FXML
    private DatePicker arrivalDatePicker;

    @FXML
    private DatePicker departureDatePicker;

    @FXML
    private TextField freeBreakfastTextField;

    @FXML
    private TextField depositPaidTextField;

    @FXML
    private TextField resCostTextField;

    @FXML
    private TextField bookingIDTextField;

    @FXML
    private Button okButton;

    private ReservationQuery reservationQuery = new ReservationQuery();
    Reservation res;
    private ObservableList<Reservation> resData = FXCollections.observableArrayList();

    StaffActivityQuery staffActivityQuery = new StaffActivityQuery();
    StaffActivity staffActivity;

    private ObservableList<StaffActivity> staffActivityData = FXCollections.observableArrayList();

    private Reservation r;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        refCodeTextField.setText(reservationQuery.getRefCode());
        roomNumberComboBox.setValue(reservationQuery.getRoomNumber());
        guestIDComboBox.setValue(reservationQuery.getGuestID());
        numGuestTextField.setText(reservationQuery.getNumPeople());
        specialCheckInComboBox.setValue(reservationQuery.getAdjustedDate());
        arrivalDatePicker.setValue(utilToLocalDate(reservationQuery.getCheckIn()));
        departureDatePicker.setValue(utilToLocalDate(reservationQuery.getCheckOut()));
        freeBreakfastTextField.setText(reservationQuery.getFreeBreakfast());
        depositPaidTextField.setText("" + reservationQuery.getDepositPaid());
        resCostTextField.setText("" + reservationQuery.getResCost());
        bookingIDTextField.setText("" + reservationQuery.getBookingID());
    }
    
    
     /**
     * Converts a java.util.Date object into a LocalDate object of the same
     * date.
     *
     * @param java.util.Date The java.util.Date to convert into a local datee object
     * @return The equivalent value in LocalDate form
     */
    private LocalDate utilToLocalDate(java.util.Date date) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        LocalDate local = LocalDate.of(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));
        return local;
    }

    /**
     * Initialises fields in StaffActivity class, then sends it through to the
     * StaffLog table which is created in the Database SetUp
     *
     * @param
     * @return
     */
    private void createActivityRecord() {

        DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        Date dateobj = new Date();
        Calendar calobj = Calendar.getInstance();

        String date = ("" + df.format(dateobj) + df.format(calobj.getTime()));

        String username = refCodeTextField.getText();
        String activityType = "CREATE RESERVATION";
        String bookingID = bookingIDTextField.getText();
        String guestID = null;

        StaffActivity entry = new StaffActivity(username, activityType, date, bookingID, guestID);

        staffActivityData.add(entry);
        staffActivityQuery.insertStaffActivity(entry);

    }

    @FXML
    private void okButtonHandle(ActionEvent event) throws IOException {

        createActivityRecord();

        Parent root = FXMLLoader.load(getClass().getResource("ReservationView.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);

        app_stage.show();
    }
}
