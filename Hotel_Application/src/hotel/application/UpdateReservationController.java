/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application;

import Model.Reservation;
import Model.Staff;
import Model.StaffActivity;
import hotel.application.database.GuestQuery;
import hotel.application.database.ReservationQuery;
import hotel.application.database.RoomViewQuery;
import hotel.application.database.StaffActivityQuery;
import hotel.application.database.StaffQuery;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Tranny
 */
public class UpdateReservationController implements Initializable {

    Stage prevStage;

    @FXML
    private TextField refCodeTextField;

    @FXML
    private ComboBox roomNumberComboBox;

    @FXML
    private ComboBox guestIDComboBox;

    @FXML
    private TextField numGuestTextField;

    @FXML
    private ComboBox specialCheckInComboBox;

    @FXML
    private DatePicker arrivalDatePicker;

    @FXML
    private DatePicker departureDatePicker;

    @FXML
    private TextField freeBreakfastTextField;

    @FXML
    private TextField depositPaidTextField;

    @FXML
    private TextField resCostTextField;

    @FXML
    private Button saveAndCompleteButton;

    @FXML
    private Button cancelButton;

    @FXML
    private TextField idTextFieldInvis;

    @FXML
    private Label roomListLabel;

    ReservationQuery reservationQuery = new ReservationQuery();

    Reservation res;
    boolean isUpdate = false;
    private Reservation toUpdate;

    private ObservableList<Reservation> resData = FXCollections.observableArrayList();

    StaffActivityQuery staffActivityQuery = new StaffActivityQuery();
    StaffActivity staffActivity;

    private ObservableList<StaffActivity> staffActivityData = FXCollections.observableArrayList();

    StaffQuery staffQuery = new StaffQuery();
    Staff staff;
    private ObservableList<Staff> staffData = FXCollections.observableArrayList();

    @FXML
    private void cancelButtonHandle(ActionEvent event) throws IOException {

        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();

    }

     /**
     * Updates an existing reservation
     */
    @FXML
    private void updateReservation() {

        toUpdate.setRoomNumber(roomNumberComboBox.getValue().toString());
        toUpdate.setGuestID((Integer) guestIDComboBox.getValue());
        toUpdate.setNumPeople(numGuestTextField.getText());
        toUpdate.setRefCode(refCodeTextField.getText());
        toUpdate.setDepositPaid(Integer.parseInt(depositPaidTextField.getText()));
        toUpdate.setResCost(Integer.parseInt(resCostTextField.getText()));

        toUpdate.setRoomNumber(roomNumberComboBox.getValue().toString());
        toUpdate.setGuestID((Integer) guestIDComboBox.getValue());
        toUpdate.setNumPeople(numGuestTextField.getText());
        java.util.Date date = Date.from(arrivalDatePicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());

        toUpdate.setCheckIn(new java.sql.Date(date.getTime()));

        java.util.Date date2 = Date.from(departureDatePicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());

        toUpdate.setCheckOut(new java.sql.Date(date2.getTime()));

        toUpdate.setDepositPaid(Integer.parseInt(depositPaidTextField.getText()));
        toUpdate.setResCost(Integer.parseInt(resCostTextField.getText()));

        System.out.println("Updating to: " + toUpdate);
        reservationQuery.updateReservation(toUpdate);

    }

     /**
     * Gets the entered information from the reservation create page and adds it to a list to create a new reservation
     */
    @FXML
    private void createReservation() {

        String refCode = refCodeTextField.getText();
        String roomNum = (String) roomNumberComboBox.getValue();
        Integer guestID = (Integer) guestIDComboBox.getValue();
        String numPeople = numGuestTextField.getText();
        LocalDate checkIn = arrivalDatePicker.getValue();
        java.util.Date arrivalDate = java.util.Date.from(checkIn.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());;

        LocalDate checkOut = departureDatePicker.getValue();
        java.util.Date checkOutDate = java.util.Date.from(checkOut.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());;

        String breakfast = freeBreakfastTextField.getText();

        String adjustedCheck = (String) specialCheckInComboBox.getValue();
        Integer deposit = Integer.parseInt(depositPaidTextField.getText());
        Integer cost = Integer.parseInt(resCostTextField.getText());

        Reservation entry = new Reservation(refCode, guestID, numPeople, roomNum, new java.sql.Date(arrivalDate.getTime()), new java.sql.Date(checkOutDate.getTime()), breakfast, adjustedCheck, deposit, cost);

        resData.add(entry);
        reservationQuery.insertReservation(entry);
    }

    public void onSubmit(ActionEvent event) throws IOException {

        updateReservation();

        createActivityRecord();

        prevStage.close();

        Parent root = FXMLLoader.load(getClass().getResource("ReservationView.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();

    }

     /**
     * Converts a java.util.Date object into a LocalDate object of the same
     * date.
     *
     * @param java.util.Date The util date to convert into a LocalDate object
     * @return The equivalent value in LocalDate form
     */
    private LocalDate utilToLocalDate(java.util.Date date) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        LocalDate local = LocalDate.of(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));
        return local;
    }
    
     /**
     * Pre-populates the fields in the createReservation Pa
     */
    public void initData(Reservation r) {
        if (r != null) {
            toUpdate = r;

            idTextFieldInvis.setText("" + r.getID());
            refCodeTextField.setText(staffQuery.getUsername());
            numGuestTextField.setText(r.getNumPeople());
            guestIDComboBox.setValue(r.getGuestID());
            roomNumberComboBox.setValue(r.getRoomNumber());

            this.arrivalDatePicker.setValue(utilToLocalDate(r.getCheckIn()));
            this.departureDatePicker.setValue(utilToLocalDate(r.getCheckOut()));
            refCodeTextField.setDisable(true);
            specialCheckInComboBox.setValue(r.getAdjustedCheck());
            freeBreakfastTextField.setText(r.getFreeBreakfast());
            depositPaidTextField.setText("" + r.getDepositPaid());
            resCostTextField.setText("" + r.getResCost());
            isUpdate = true;
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        //Combo Box options for room numbers
        ObservableList<String> roomNumOptions = FXCollections.observableArrayList();

        RoomViewQuery rq = new RoomViewQuery();
        roomNumOptions.addAll(rq.getRoomNums());

        roomNumberComboBox.setItems(roomNumOptions);

        // Combo Box options for guestID
        ObservableList<Integer> guestIDOptions = FXCollections.observableArrayList();

        GuestQuery gq = new GuestQuery();
        guestIDOptions.addAll(gq.getGuestIDs());
        guestIDComboBox.setItems(guestIDOptions);

    }

    /**
     * Converts a LocalDate object into a java.util.Date object of the same
     * date.
     *
     * @param localDate The local date to convert into a java.util.Date object
     * @return The equivalent value in java.util.Date form
     */
    private Date localDateToUtilDate(LocalDate localDate) {
        GregorianCalendar cal = new GregorianCalendar(
                localDate.getYear(), localDate.getMonthValue() - 1, localDate.getDayOfMonth());
        Date date = cal.getTime();
        return date;
    }

    ObservableList<String> options = FXCollections.observableArrayList();

    @FXML
    private void checkInOutComboClicked(MouseEvent event) {

        String selectedRoom = this.roomNumberComboBox.getSelectionModel().getSelectedItem().toString();
        LocalDate inDate = this.arrivalDatePicker.getValue();
        LocalDate outDate = this.departureDatePicker.getValue();
        ReservationQuery reservationQuery1 = new ReservationQuery();

        if (inDate != null && outDate != null && selectedRoom != null) {

            java.util.Date inDateUtil = this.localDateToUtilDate(inDate);
            java.util.Date outDateUtil = this.localDateToUtilDate(outDate);

            boolean isEarlyCheckInAvailable = reservationQuery1.isRoomAvailableThatMorning(
                    new java.sql.Date(inDateUtil.getTime()), Integer.parseInt(selectedRoom));
            boolean isLateCheckOutAvailable = reservationQuery1.isRoomAvailableThatAfternoon(
                    new java.sql.Date(outDateUtil.getTime()), Integer.parseInt(selectedRoom));

            if (isEarlyCheckInAvailable) {
                options.add("Early");

            }
            if (isLateCheckOutAvailable) {
                options.add("Late");

            }
            if (isEarlyCheckInAvailable && isLateCheckOutAvailable) {

                options.add("Both");

            }

        }

    }

    @FXML
    private void roomListHandle(Event event) throws IOException {
        Stage popup = new Stage();
        popup.initModality(Modality.WINDOW_MODAL);
        Parent root = FXMLLoader.load(getClass().getResource("RoomView.fxml"));
        Scene scene = new Scene(root);
        popup.setScene(scene);
        popup.show();

    }

     /**
     * Initialises fields in StaffActivity class, then sends it through to the StaffLog
     * table which is created in the Database SetUp
     *
     * @param 
     * @return
     */
    @FXML
    private void createActivityRecord() {

        DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        Date dateobj = new Date();
        Calendar calobj = Calendar.getInstance();

        String date = ("" + df.format(dateobj) + df.format(calobj.getTime()));

        String username = refCodeTextField.getText();
        String activityType = "UPDATE RESERVATION";

        String bookingID = idTextFieldInvis.getText();
        String guestID = null;

        StaffActivity entry = new StaffActivity(username, activityType, date, bookingID, guestID);

        staffActivityData.add(entry);
        staffActivityQuery.insertStaffActivity(entry);

    }

    void setStage(Stage oldStage) {
        prevStage = oldStage;
    }
}
