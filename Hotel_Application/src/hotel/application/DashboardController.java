/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application;

import Model.Staff;
import hotel.application.database.StaffQuery;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Tranny
 */
public class DashboardController implements Initializable {
Stage prevStage;

    @FXML
    private Pane customersPaneButton;

    @FXML
    private Pane roomsPaneButton;

    @FXML
    private Pane purchasesPaneButton;

    @FXML
    private Pane staffPaneButton;
    
    @FXML
    private Pane reservationPaneButton;
    
    @FXML

    private Label loggedInUser;
    
    StaffQuery staffQuery = new StaffQuery();
    Staff staff;
    private ObservableList<Staff> staffData = FXCollections.observableArrayList();
    
    private Staff s;

    private TextField loggedInUserTextField;


    @FXML
    private void handleReservationButton(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("ReservationView.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();
 
    }


    @FXML
    private void handleRoomButton(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("RoomView.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();
    }
    
    @FXML
    private void handleCustomerButton(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("CustomerView.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();
    }

    @FXML
        private void handlePurchasesButton(Event event) throws IOException {         
        Parent root = FXMLLoader.load(getClass().getResource("Purchases.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = new Stage();
        app_stage.setScene(scene);
        app_stage.show();
    }

    @FXML
        private void handleStaffButton(Event event) throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("StaffActivityLog.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();
    }

    @FXML
        private void handleLogOutButton(Event event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("LoginPage.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();
    
    }  
            
    @Override
        public void initialize(URL url, ResourceBundle rb) {
            
          loggedInUser.setText(staffQuery.getUsername());

    }   
        
void setStage(Stage oldStage){
    prevStage = oldStage;
}   
}
