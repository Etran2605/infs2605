package hotel.application;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import Model.Guest;
import Model.Reservation;
import Model.Staff;
import Model.StaffActivity;
import hotel.application.database.GuestQuery;
import hotel.application.database.ReservationQuery;
import hotel.application.database.StaffActivityQuery;
import hotel.application.database.StaffQuery;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Tranny
 */
public class UpdateCustomerController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        staffRefCode.setText(staffQuery.getUsername());
    }

    @FXML
    private TextField guestIDTextfield;

    @FXML
    private TextField fNameTextfield;

    @FXML
    private TextField lNameTextfield;

    @FXML
    private TextField emailAddTextfield;

    @FXML
    private TextArea postalAddTextArea;

    @FXML
    private TextField contactNumberTextfield;

    @FXML
    private Button createButton;

    @FXML
    private Button backButton;

    @FXML
    private TextField staffRefCode;

    StaffQuery staffQuery = new StaffQuery();
    Staff staff;
    private ObservableList<Staff> staffData = FXCollections.observableArrayList();

    GuestQuery guestQuery = new GuestQuery();

    boolean isUpdate = false;
    private Guest toUpdate;

    Stage prevStage;

    StaffActivityQuery staffActivityQuery = new StaffActivityQuery();
    StaffActivity staffActivity;
    private ObservableList<StaffActivity> staffActivityData = FXCollections.observableArrayList();

    private ObservableList<Guest> guestData = FXCollections.observableArrayList();

    @FXML
    private void backButtonHandle(ActionEvent event) throws IOException {

        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();

    }

    /**
     * Gets the entered information from the customerCreate page and adds it to
     * the database
     */
    @FXML
    private void createCustomer() {

        Integer guestID = Integer.parseInt(guestIDTextfield.getText());
        String firstName = fNameTextfield.getText();
        String lastName = lNameTextfield.getText();
        String email = emailAddTextfield.getText();
        String address = postalAddTextArea.getText();
        String phone = contactNumberTextfield.getText();

        Guest entry = new Guest(guestID, firstName, lastName, email, address, phone);

        guestData.add(entry);
        guestQuery.insertGuest(entry);
    }

    @FXML
    public void onSaveButton(ActionEvent event) throws IOException {

        updateCustomer();

        createActivityRecord();

        prevStage.close();

        Parent root = FXMLLoader.load(getClass().getResource("CustomerView.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();
    }

    /**
     * Updates an existing customer's information on the database
     */
    @FXML
    private void updateCustomer() {

        toUpdate.setFname(fNameTextfield.getText());
        toUpdate.setLname(lNameTextfield.getText());
        toUpdate.setEmail(emailAddTextfield.getText());
        toUpdate.setAddress(postalAddTextArea.getText());
        toUpdate.setPhone(contactNumberTextfield.getText());

        String fname = fNameTextfield.getText();
        String lname = lNameTextfield.getText();
        String email = emailAddTextfield.getText();
        String address = postalAddTextArea.getText();
        String phone = contactNumberTextfield.getText();

        System.out.println("Updating to: " + toUpdate);
        guestQuery.updateGuest(toUpdate);

    }

    /**
     * Pre-populates the fields of create customer page after you click the
     * "update" button
     *
     * @param Guest The guest model
     * @return isUpdate = true
     */
    void initData(Guest c) {
        if (c != null) {
            toUpdate = c;

            guestIDTextfield.setText(Integer.toString(c.getGuestID()));
            fNameTextfield.setText(c.getFname());
            lNameTextfield.setText(c.getLname());
            emailAddTextfield.setText(c.getEmail());
            postalAddTextArea.setText(c.getAddress());
            contactNumberTextfield.setText(c.getPhone());

            isUpdate = true;
        }

    }

    void setStage(Stage oldStage) {
        prevStage = oldStage;
    }

    /**
     * Initialises fields in StaffActivity class, then sends it through to the
     * StaffLog table which is created in the Database SetUp
     *
     * @param
     * @return
     */
    private void createActivityRecord() {

        DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        Date dateobj = new Date();
        Calendar calobj = Calendar.getInstance();

        String date = ("" + df.format(dateobj) + df.format(calobj.getTime()));

        String username = staffRefCode.getText();
        String activityType = "UPDATE CUSTOMER";
        String bookingID = null;
        String guestID = guestIDTextfield.getText();

        StaffActivity entry = new StaffActivity(username, activityType, date, bookingID, guestID);

        staffActivityData.add(entry);
        staffActivityQuery.insertStaffActivity(entry);

    }

}
