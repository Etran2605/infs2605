/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application;

import Model.Guest;
import Model.Reservation;
import Model.Staff;
import Model.StaffActivity;
import hotel.application.database.GuestQuery;
import hotel.application.database.ReservationQuery;
import hotel.application.database.RoomViewQuery;
import hotel.application.database.StaffActivityQuery;
import hotel.application.database.StaffQuery;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author Tranny
 */
public class CustomerViewController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private TableView customerTableView;

    @FXML
    private TableColumn guestIDColumn;

    @FXML
    private TableColumn firstNameColumn;

    @FXML
    private TableColumn lastNameColumn;

    @FXML
    private TableColumn emailAddressColumn;

    @FXML
    private TableColumn contactNumberColumn;

    @FXML
    private TableColumn postalAddressColumn;

    @FXML
    private Pane createPaneButton;

    @FXML
    private Pane deletePaneButton;

    @FXML
    private Pane updatePaneButton;

    @FXML
    private Pane dashboardPaneButton;

    @FXML
    private Label loggedInUser;

    Stage prevStage;

    StaffQuery staffQuery = new StaffQuery();
    Staff staff;
    private ObservableList<Staff> staffData = FXCollections.observableArrayList();

    private Guest toDelete;

    GuestQuery guestQuery = new GuestQuery();
    Guest guest;

    private ObservableList<Guest> guestData = FXCollections.observableArrayList();

    StaffActivityQuery staffActivityQuery = new StaffActivityQuery();
    StaffActivity staffActivity;

    private ObservableList<StaffActivity> staffActivityData = FXCollections.observableArrayList();

    @FXML
    private void createHyperlinkHandle(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("CustomerCreate.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();
    }

    @FXML
    private void updateHyperlinkHandler(Event event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("UpdateCustomer.fxml"));
        Parent root = (Parent) loader.load();
        UpdateCustomerController controller = (UpdateCustomerController) loader.getController();
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        controller.setStage(app_stage);

        UpdateCustomerController controller2
                = loader.<UpdateCustomerController>getController();

        controller2.initData((Guest) customerTableView.getSelectionModel().getSelectedItem());

        Stage popup = new Stage();
        Scene scene = new Scene(root);
        popup.setScene(scene);
        popup.show();
    }

    @FXML
    private void deleteHyperlinkHandle(Event event) throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("CustomerView.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();

        Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.WINDOW_MODAL);
        Button yes = new Button("YES");
        Button no = new Button("NO");
        no.setDefaultButton(true);

        yes.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {

                GuestQuery gq = new GuestQuery();
                gq.deleteGuest((Guest) customerTableView.getSelectionModel().getSelectedItem());

                dialogStage.close();

                app_stage.close();

                try {
                    Parent root = FXMLLoader.load(getClass().getResource("CustomerView.fxml"));
                    Scene scene = new Scene(root);
                    Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    app_stage.hide();
                    app_stage.setScene(scene);
                    app_stage.show();
                } catch (IOException ex) {
                    Logger.getLogger(CustomerViewController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        no.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                dialogStage.close();
            }

        });

        dialogStage.setScene(new Scene(VBoxBuilder.create().
                children(new Text("Are you sure you want to DELETE"), yes, no).
                alignment(Pos.CENTER).padding(new Insets(5)).build()));
        dialogStage.show();

    }

    @FXML
    private void dashboardHyperlinkHandle(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("Dashboard.fxml"));
        Scene scene = new Scene(root);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(scene);
        app_stage.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        guestIDColumn.setCellValueFactory(new PropertyValueFactory<>("guestID"));
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<Guest, String>("fname"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<Guest, String>("lname"));
        emailAddressColumn.setCellValueFactory(new PropertyValueFactory<Guest, String>("email"));
        postalAddressColumn.setCellValueFactory(new PropertyValueFactory<Guest, String>("address"));
        contactNumberColumn.setCellValueFactory(new PropertyValueFactory<>("phone"));

        guestData.addAll(guestQuery.getGuest());
        customerTableView.setItems(guestData);

        loggedInUser.setText(staffQuery.getUsername());


    }

    void setStage(Stage oldStage) {
        prevStage = oldStage;
    }

}
