/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author renazhou
 */
public class Guest {

    int guestID;
    String fname;
    String lname;
    String email;
    String address;
    String phone;

    public Guest(String fname, String lname, String email, String address, String phone) {
        this.fname = fname;
        this.lname = lname;
        this.email = email;
        this.address = address;
        this.phone = phone;
    }

    public Guest(Integer guestID, String firstName, String lastName, String email, String address, String phone) {
        this.guestID = guestID;
        this.fname = firstName;
        this.lname = lastName;
        this.email = email;
        this.address = address;
        this.phone = phone;
    }

    public Guest(String fname, String lname) {
        this.fname = fname;
        this.lname = lname;

    }

    public int getGuestID() {
        return guestID;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setGuestId(int id) {
        this.guestID = id;
    }

    @Override
    public String toString() {
        return "Guest{" + "guestID=" + guestID + ", fname=" + fname + ", lname=" + lname + ", email=" + email + ", address=" + address + ", phone=" + phone + '}';
    }

}
