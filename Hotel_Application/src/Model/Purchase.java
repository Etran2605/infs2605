/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Tranny
 */
public class Purchase {

    int purchaseQuantity;
    Integer resId;
    String purchaseDesc;
    Double purchaseCost;

    public Purchase(int purchaseQuantity, Integer resId, String purchaseDesc, Double purchaseCost) {
        this.purchaseQuantity = purchaseQuantity;
        this.purchaseDesc = purchaseDesc;
        this.purchaseCost = purchaseCost;
        this.resId = resId;
    }

    public Integer getResId() {
        return resId;
    }

    public int getPurchaseQuantity() {
        return purchaseQuantity;
    }

    public String getPurchaseDesc() {
        return purchaseDesc;
    }

    public Double getPurchaseCost() {
        return purchaseCost;
    }

    public void setPurchaseQuantity(int purchaseQuantity) {
        this.purchaseQuantity = purchaseQuantity;
    }

    public void setPurchaseDesc(String purchaseDesc) {
        this.purchaseDesc = purchaseDesc;
    }

    public void setPurchaseCost(Double purchaseCost) {
        this.purchaseCost = purchaseCost;
    }
    /*
     public void setTotalPurchaseCostForItem(Double totalPurchaseCostForItem) {
     this.totalPurchaseCostForItem = totalPurchaseCostForItem;
     }

     public void setTotalCost(Double totalCost) {
     totalCost = 
     this.totalCost = totalCost;
     }
     */

    @Override
    public String toString() {
        return purchaseQuantity + "x " + purchaseDesc + " " + "$" + purchaseCost + "each";
    }

}
