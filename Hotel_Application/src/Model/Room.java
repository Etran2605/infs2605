/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Date;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author renazhou
 */
public class Room {

    StringProperty number;
    StringProperty name;
    StringProperty description;
    IntegerProperty costPerNight;

    public Room(String number, String name, String description, Integer costPerNight) {
        this.number = new SimpleStringProperty(number);
        this.name = new SimpleStringProperty(name);
        this.description = new SimpleStringProperty(description);
        this.costPerNight = new SimpleIntegerProperty(costPerNight);

    }

    public StringProperty getNumber() {
        return number;
    }

    public StringProperty getName() {
        return name;
    }

    public StringProperty getDescription() {
        return description;
    }

    public IntegerProperty getCostPerNight() {
        return costPerNight;
    }

    public void setNumber(StringProperty number) {
        this.number = number;
    }

    public void setName(StringProperty name) {
        this.name = name;
    }

    public void setDescription(StringProperty description) {
        this.description = description;
    }

    public void setCostPerNight(IntegerProperty costPerNight) {
        this.costPerNight = costPerNight;
    }

}
