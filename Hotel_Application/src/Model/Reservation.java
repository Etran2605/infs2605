/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;

import java.util.logging.Logger;
import javafx.beans.property.IntegerProperty;
import javafx.scene.control.TableColumn;
import javafx.stage.Stage;

/**
 *
 * @author renazhou
 */
public class Reservation {
    int ID;
    String refCode;
    Integer guestID;
    String numPeople;
    String roomNumber;
    Date checkIn;
    Date checkOut;
    String adjustedCheck;
    String freeBreakfast;
    Integer depositPaid;
    Integer resCost;
    Integer amountOwing;

    public Reservation(Integer id, String refCode, Integer guestID, String numPeople, String roomNumber, Date checkIn, Date checkOut, String adjustedCheck, String freeBreakfast, Integer depositPaid) {
        this.ID = id;
        this.refCode = refCode;
        this.guestID = guestID;
        this.numPeople = numPeople;
        this.roomNumber = roomNumber;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.adjustedCheck = adjustedCheck;
        this.freeBreakfast = freeBreakfast;
        this.depositPaid = depositPaid;


    }


    public Reservation(Integer id, String refCode, Integer guestID, String numPeople, String roomNumber, Date checkIn, Date checkOut, String adjustedCheck, String freeBreakfast, Integer depositPaid, Integer resCost/*, Integer amountOwing*/) {

        this.ID = id;
        this.refCode = refCode;
        this.guestID = guestID;
        this.numPeople = numPeople;
        this.roomNumber = roomNumber;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.adjustedCheck = adjustedCheck;
        this.freeBreakfast = freeBreakfast;
        this.depositPaid = depositPaid;
        this.resCost = resCost;
        this.amountOwing = resCost - depositPaid;

    }

    public Reservation(String refCode, Integer guestID, String numPeople, String roomNum, Date checkIn, Date checkOut, String adjustedCheck, String freeBreakfast, Integer deposit, Integer cost) {
        this.refCode = refCode;
        this.guestID = guestID;
        this.numPeople = numPeople;
        this.roomNumber = roomNum;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.adjustedCheck = adjustedCheck;
        this.freeBreakfast = freeBreakfast;
        this.depositPaid = deposit;
        this.resCost = cost;
        this.amountOwing = cost - deposit;
    }

    public int getID() {
        return ID;
    }


    public String getRefCode() {
        return refCode;
    }

    public Integer getGuestID() {
        return guestID;
    }

    public String getNumPeople() {
        return numPeople;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public Date getCheckIn() {
        return checkIn;
    }

    public Date getCheckOut() {
        return checkOut;
    }

    public String getAdjustedCheck() {
        return adjustedCheck;
    }

    public Integer getAmountOwing(){
        return amountOwing;
    }

    public void setAdjustedCheck(String adjustedCheck) {
        this.adjustedCheck = adjustedCheck;
    }


    public String getFreeBreakfast() {
        return freeBreakfast;
    }

    public Integer getDepositPaid() {
        return depositPaid;
    }

    public Integer getResCost(){
        return resCost;

    }


    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    public void setName(Integer guestID) {
        this.guestID = guestID;
    }

    public void setNumPeople(String numPeople) {
        this.numPeople = numPeople;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public void setCheckIn(Date checkIn) {
        this.checkIn = checkIn;
    }

    public void setCheckOut(Date checkOut) {
        this.checkOut = checkOut;
    }

    public void setGuestID(Integer guestID) {
        this.guestID = guestID;
    }


    public void setFreeBreakfast(String freeBreakfast) {
        this.freeBreakfast = freeBreakfast;
    }

    public void setDepositPaid(Integer depositPaid) {
        this.depositPaid = depositPaid;
    }

    public void setResCost(Integer resCost){
        this.resCost = resCost;
    }


    public void setAmountOwing(Integer resCost, Integer depositPaid){
        this.amountOwing = resCost - depositPaid;
    }


    @Override
    public String toString() {

        return "Reservation{" + "ID=" + ID + ", refCode=" + refCode + ", guestID=" + guestID + ", numPeople=" + numPeople + ", roomNumber=" + roomNumber + ", checkIn=" + checkIn + ", checkOut=" + checkOut + ", adjustedCheck=" + adjustedCheck + ", freeBreakfast=" + freeBreakfast + ", depositPaid=" + depositPaid + ", resCost=" + resCost + ",amountOwing=" +'}';
    }



    public void setId(int aInt) {
        ID = aInt;
    }



}
