/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Tranny
 */
public class StaffActivity {

    int activityID;
    String username;
    String activityType;
    String date;
    String bookingID;
    String guestID;

    public StaffActivity(Integer activityID, String name, String activityType, String date, String bookingID, String guestID) {
        this.activityID = activityID;
        this.username = name;
        this.activityType = activityType;
        this.date = date;
        this.bookingID = bookingID;
        this.guestID = guestID;
    }

    public StaffActivity(String name, String activityType, String date, String bookingID, String guestID) {
        this.username = name;
        this.activityType = activityType;
        this.date = date;
        this.bookingID = bookingID;
        this.guestID = guestID;
    }

    public StaffActivity(String name, String activityType, String date, String bookingID) {
        this.username = name;
        this.activityType = activityType;
        this.date = date;
        this.bookingID = bookingID;

    }

    public StaffActivity(String username, String activityType, String date) {
        this.username = username;
        this.activityType = activityType;
        this.date = date;
    }

    public int getActivityID() {
        return activityID;
    }

    public String getName() {
        return username;
    }

    public String getActivityType() {
        return activityType;
    }

    public String getDate() {
        return date;
    }

    public String getBookingID() {
        return bookingID;
    }

    public String getGuestID() {
        return guestID;
    }

    public void setActivityID(int activityID) {
        this.activityID = activityID;
    }

    public void setName(String name) {
        this.username = name;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public void setBookingID(String bookingID) {
        this.bookingID = bookingID;
    }

    public void setGuestID(String guestID) {
        this.guestID = guestID;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
