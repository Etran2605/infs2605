/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author renazhou
 */
public class Staff {

    private Integer id;
    private String username;
    private String password;

    public Staff(Integer id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public Staff(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Staff(String username) {
        this.username = username;
    }

    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
